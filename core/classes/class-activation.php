<?php

namespace Astro_Dynamic;

class Activation
{

    public $plugin_name = 'Astro Dynamic';
    public $plugin_slug = 'astro_dynamic';
    public $docs_url = 'https://webforia.id/docs/astro-dynamic';

    public function __construct()
    {
        $this->check_license();

        add_action('admin_notices', array($this, 'theme_notice'));
        // add_action('after_switch_theme', array($this, 'deactive'));

    }

    /**
     * Check key from server
     *
     * @return void
     */
    public function check_license()
    {
        if ($this->get_status() !== 'active') {

            $key = get_option("{$this->plugin_slug}_license_key"); // get valid email and key form option database

            $request = wp_remote_get('https://account.ratakan.com/api/check-access/by-email?_key=SSbXulRIpEzY9vD5rKr7&email=' . $key);

            if (is_wp_error($request)) {
                return false;
            }
            $body = wp_remote_retrieve_body($request);
            $data_api = json_decode($body);

            if ($data_api->ok) {
                update_option("{$this->plugin_slug}_license_status", 'active');
            } else {
                update_option("{$this->plugin_slug}_license_status", 'deactive');
            }

        }

    }

    /**
     * Check status on option
     */

    public function get_status(){
        return get_option("{$this->plugin_slug}_license_status");
    }

    /** Set license status after theme change */
    public function deactive()
    {
        delete_option("{$this->plugin_slug}_license_status", 'deactive');
    }

   
    /**
     * Show notif
     *
     * @return void
     */
    public function theme_notice()
    {

        // first notif 
        if (empty($this->get_status())):

            $error = '<p>' . __("Please activate your {$this->plugin_name} license to get feature updates, premium support <a href='" . admin_url('admin.php?page=astro-license') . "'>Activate License</a>", "rt_domain") . '</p>';

            echo Notice::get_error_notice($error);
        endif;

        // if license fail
        if ($this->get_status() == 'deactive'):
            $error = '<p>' . __("Activation {$this->plugin_name} failed, please check your lisence key <a href='" . admin_url('admin.php?page=astro-license') . "'>Activate License</a>", "rt_domain") . '</p>';

            echo Notice::get_error_notice($error);

        endif;

        // if license success
        if ($this->get_status() == 'active' && get_option("{$this->plugin_slug}-lisense-dismiss") != true):
            $success = '<p>' . __("Activation {$this->plugin_name} success", "rt_domain") . '</p>
				                        <span style="display: block; margin: 0.5em  0; clear: both;">
				                            <a href="' . $this->docs_url . '" target="_blank">Visit Tutorial</a>
				                        </span>';

            echo Notice::get_success_notice($success, "{$this->plugin_slug}-lisense-dismiss");
            Notice::update_notice_option("{$this->plugin_slug}-lisense-dismiss");

        endif;
    }

    public static function active()
    {
        return (get_option("astro_blog_license_status") === 'active') ? true : false;
    }

}
new Activation;

