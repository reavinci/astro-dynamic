<?php
function rt_added_cpt(){
    if (function_exists('acf_add_options_page')) {

        acf_add_options_page(array(
            'page_title' => 'CPT Panel',
            'menu_title' => 'CPT Panel',
            'menu_slug' => 'astro-dynamic-setting',
            'parent_slug' => 'options-general.php',
            'capability' => 'edit_posts',
            'redirect' => false,
        ));
    }

}
add_action('after_setup_theme', 'rt_added_cpt');

if (get_field('cpt_portfolio', 'option')) {
    include_once dirname(__FILE__) . '/cpt-portfolio.php';
}
if (get_field('cpt_testimonial', 'option')) {
    include_once dirname(__FILE__) . '/cpt-testimonial.php';
}
if (get_field('cpt_slider', 'option')) {
    include_once dirname(__FILE__) . '/cpt-slider.php';
}
if (get_field('cpt_team', 'option')) {
    include_once dirname(__FILE__) . '/cpt-team.php';
}
