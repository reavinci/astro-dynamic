<?php
/*=================================================;
/* CPT
/*================================================= */
function rt_register_cpt_portfolio()
{

    /**
     * Post Type: Portfolios.
     */

    $labels = array(
        "name" => __("Portfolios", "rt_domain"),
        "singular_name" => __("Portfolio", "rt_domain"),
    );

    $args = array(
        "label" => __("Portfolios", "rt_domain"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "portfolio", "with_front" => true),
        "query_var" => true,
        "menu_icon" => "dashicons-art",
        "supports" => array("title", "editor", "thumbnail"),
    );

    register_post_type("portfolio", $args);
}

add_action('init', 'rt_register_cpt_portfolio');

/*=================================================;
/* CATEGORY
/*================================================= */
function rt_register_portfolio_category()
{

    /**
     * Taxonomy: Categories.
     */

    $labels = array(
        "name" => __("Categories", "rt_domain"),
        "singular_name" => __("Category", "rt_domain"),
    );

    $args = array(
        "label" => __("Categories", "rt_domain"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'portfolio_category', 'with_front' => true),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "portfolio_category",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy("portfolio_category", array("portfolio"), $args);
}
add_action('init', 'rt_register_portfolio_category');

/*=================================================;
/* TAGS
/*================================================= */
function rt_register_portfolio_tag()
{

    /**
     * Taxonomy: Tags.
     */

    $labels = array(
        "name" => __("Tags", "rt_domain"),
        "singular_name" => __("Tag", "rt_domain"),
    );

    $args = array(
        "label" => __("Tags", "rt_domain"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'portfolio_tag', 'with_front' => true),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "portfolio_tag",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy("portfolio_tag", array("portfolio"), $args);
}
add_action('init', 'rt_register_portfolio_tag');





