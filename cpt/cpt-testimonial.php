<?php
function rt_register_cpt_testimonial()
{

    /**
     * Post Type: Testimonials.
     */

    $labels = array(
        "name" => __("Testimonials", "rt_domain"),
        "singular_name" => __("Testimonial", "rt_domain"),
        "featured_image" => __("Profile Picture", "rt_domain"),
        "set_featured_image" => __("Set Profile Picture", "rt_domain"),
        "remove_featured_image" => __("Remove Profile Picture", "rt_domain"),
        "use_featured_image" => __("Use as Profile Picture", "rt_domain"),
    );

    $args = array(
        "label" => __("Testimonials", "rt_domain"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "testimonial", "with_front" => true),
        "query_var" => true,
        "menu_icon" => "dashicons-format-quote",
        "supports" => array("title", "thumbnail"),
    );

    register_post_type("testimonial", $args);
}

add_action('init', 'rt_register_cpt_testimonial');

/*=================================================;
/* CATEGORY
/*================================================= */
function rt_register_testimonial_category()
{

    /**
     * Taxonomy: Categories.
     */

    $labels = array(
        "name" => __("Categories", "rt_domain"),
        "singular_name" => __("Category", "rt_domain"),
    );

    $args = array(
        "label" => __("Categories", "rt_domain"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array('slug' => 'testimonial_category', 'with_front' => true),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "testimonial_category",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy("testimonial_category", array("testimonial"), $args);
}
add_action('init', 'rt_register_testimonial_category');

/*=================================================;
/* TITLE
/*================================================= */
function rt_change_title_testimonial($title)
{
    $screen = get_current_screen();

    if ('testimonial' == $screen->post_type) {
        $title = 'Enter name';
    }

    return $title;
}

add_filter('enter_title_here', 'rt_change_title_testimonial');
