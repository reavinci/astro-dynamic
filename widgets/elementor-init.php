<?php
namespace Astro_Dynamic\Elementor;

class Elementor_Init
{

    public function __construct()
    {

        add_action('elementor/widgets/widgets_registered', array($this, 'on_widgets_registered'));
        add_action('elementor/init', array($this, 'register_category'));

    }

    public function on_widgets_registered()
    {
        $this->include_part();
        $this->register_widget();
    }

    public function include_part()
    {

        include dirname(__FILE__) . '/slider/slider.php';
        include dirname(__FILE__) . '/portfolio/portfolio.php';
        include dirname(__FILE__) . '/team/team.php';
        include dirname(__FILE__) . '/testimonial/testimonial.php';



    }

    /**
     * register Category
     * @since 1.0.0
     * @return [add new category]
     */
    public function register_category()
    {
        \Elementor\Plugin::instance()->elements_manager->add_category(
            'astro-dynamic',
            [
                'title' => 'Astro Dynamic',
                'icon' => 'font',
            ],
            1
        );
    }

    /**
     * Register Widget
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function register_widget()
    {
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Slider());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Portfolio());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Team());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Testimonial());


    }

    // end class
}

new Elementor_Init;
