 <?php $sosmed_class = ($settings["social_style"] !== "custom") ? "rt-socmed--{$settings["social_style"]}" : "";?>

<div class="flex-item">

 <div id="team-<?php echo get_the_ID()?>" <?php post_class("adc-team") ?>>

      <?php if (has_post_thumbnail() && $settings['image_size'] != 'none'): ?>
      <div class="adc-team__thumbnail rt-img rt-img--full">
        <?php the_post_thumbnail($settings['image_size'], 'img-responsive')?>
      </div>
      <?php endif;?>

      <div class="adc-team__body">

        <h3 class="adc-team__name"><?php the_title()?></h3>

        <?php if(astro_get_field('position') && $settings['setting_position']):?>
        <div  class="adc-team__position">
          <?php echo astro_get_field('position') ?>
        </div>
        <?php endif ?>
        

        <?php if(astro_get_field('description') && $settings['setting_desc']):?>
        <div class="adc-team__content">
            <?php echo astro_get_field('description')?>
        </div>
        <?php endif ?>

        <?php if(astro_get_field('phone') && $settings['setting_phone']):?>
        <div  class="adc-team__phone">
          <?php echo astro_get_field('phone') ?>
        </div>
        <?php endif ?>

        <?php if(astro_get_field('email') && $settings['setting_email']):?>
        <div  class="adc-team__email">
          <?php echo astro_get_field('email') ?>
        </div>
        <?php endif ?>


         <?php if(!empty(astro_get_field('sosmed'))): ?>

        

          <div class="rt-socmed <?php echo $sosmed_class ?>">

             <?php foreach (get_field('sosmed') as $key => $value): ?>

            <a href="<?php echo $value['url'] ?>" class="rt-socmed__item <?php echo $value['social_media'] ?>">
                  <i class="<?php echo astro_get_fontawesome_class($value['social_media']) ?>"></i>
            </a>

            <?php endforeach; ?>

          </div>
        <?php endif ?>

  
      </div>

  </div>
  
</div>